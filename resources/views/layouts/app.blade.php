<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    @livewireStyles
    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

</head>

<body class="font-sans antialiased">
    <x-jet-banner />

    <div class="min-h-screen bg-gray-100">
        @livewire('navigation-menu')

        <!-- Page Heading -->
        @if (isset($header))
            <header class="bg-white shadow">
                <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    {{ $header }}
                </div>
            </header>
        @endif

        <!-- Page Content -->
        <main>
            {{ $slot }}
        </main>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" crossorigin="anonymous">
    </script>
    <script>
        $(document).ready(function() {
            // To create the datatable for the category table
            $('#category_table').DataTable({
                ajax: {
                    url: "{{ route('category.show-category') }}",
                    dataSrc: ""
                },
                "columns": [{
                        "data": "name"
                    },
                    {
                        "data": "description"
                    },
                    {
                        "data": "action",
                        "render": function(data, type, row, meta) {
                            return '<a onclick="deleteCategory(' + row.id +
                                ')" class="btn btn-danger text-white mr-2"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a><a href="/edit-category/' +
                                row.id +
                                '" class="btn btn-success"><i class="fa fa-edit"></i> Edit</a>';
                        }
                    }
                ]
            });

            // To create the datatable for the product table
            $('#product_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('product.show-product') }}"
                },
                "columns": [{
                        "data": "product_image",
                        "render": function(data, type, row, meta) {
                            return '<a target="_blank" title="click to view image" href="' + row.product_image + '"><img src="' + row
                                .product_image +
                                '" height="50" width="50"/></a>';
                        }
                    },
                    {
                        "data": "category_id"
                    },
                    {
                        "data": "name"
                    },
                    {
                        "data": "description"
                    },
                    {
                        "data": "price"
                    },
                    {
                        "data": "action",
                        "render": function(data, type, row, meta) {
                            return '<a onclick="deleteProduct(' + row.id +
                                ')" class="btn btn-danger text-white mr-2"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a><a href="/edit-product/' +
                                row.id +
                                '" class="btn btn-success"><i class="fa fa-edit"></i> Edit</a>';
                        }
                    }
                ]
            });
        });

        // For deleting the category from the id.
        function deleteCategory(id) {
            swal({
                title: "Are you sure?",
                text: "You want to Delete.",
                icon: 'warning',
                dangerMode: true,
                buttons: true
            }).then(function(willDelete) {
                if (willDelete) {
                    $.ajax({
                        url: '/delete-category/' + id,
                        type: "get"
                    }).done(function(data) {
                        if (data.status == "200") {
                            swal(data.status, {
                                title: data.code
                            });
                        }
                        var table = $('#category_table').DataTable();
                        table.ajax.reload();
                        swal("Deleted successfully.!", {
                            icon: "success",
                        });
                    }).fail(function(jqXHR, ajaxOptions, thrownError) {
                        swal("Something wrong.", {
                            title: 'Cancelled',
                            icon: "error",
                        });
                    });
                } else {
                    swal("Record is safe", {
                        title: 'Cancelled',
                        icon: "error",
                    });
                }
            });
        }

        // For deleting the product from the id.
        function deleteProduct(id) {
            swal({
                title: "Are you sure?",
                text: "You want to Delete.",
                icon: 'warning',
                dangerMode: true,
                buttons: true
            }).then(function(willDelete) {
                if (willDelete) {
                    $.ajax({
                        url: '/delete-product/' + id,
                        type: "get"
                    }).done(function(data) {
                        if (data.status == "200") {
                            swal(data.status, {
                                title: data.code
                            });
                        }
                        var table = $('#product_table').DataTable();
                        table.ajax.reload();
                        swal("Deleted successfully.!", {
                            icon: "success",
                        });
                    }).fail(function(jqXHR, ajaxOptions, thrownError) {
                        swal("Something wrong.", {
                            title: 'Cancelled',
                            icon: "error",
                        });
                    });
                } else {
                    swal("Record is safe", {
                        title: 'Cancelled',
                        icon: "error",
                    });
                }
            });
        }

    </script>

    @stack('modals')

    @livewireScripts
</body>

</html>
