<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Category Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if(Session::has('success_msg'))
            <div class="alert alert-success" role="alert">
                <i class="fa fa-check-circle"></i> {{Session::get('success_msg')}}
            </div>
            @endif
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <a href="{{ route('category.create-category')}}" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Add New Category</a>
                        </div>
                    </div> <br />
                    <table id="category_table" class="table table-striped">
                        <thead>
                            <tr>
                                <th>Category Name</th>
                                <th>Category Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>