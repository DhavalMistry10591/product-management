<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\FacebookController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

/* 
    *Routes for Facebook authentication.
*/
Route::get('auth/facebook', [FacebookController::class, 'redirectToFacebook']);
Route::get('auth/facebook/callback', [FacebookController::class, 'facebookSignin']);

/* 
    *Routes for Category.
*/
Route::get('category-dashboard', [CategoryController::class, 'categoryDashboard'])->name('category.category-dashboard');
Route::get('create-category', [CategoryController::class, 'createCategory'])->name('category.create-category');
Route::post('add-category', [CategoryController::class, 'addCategory'])->name('category.add-category');
Route::get('show-category', [CategoryController::class, 'showCategory'])->name('category.show-category');
Route::post('update-category', [CategoryController::class, 'updateCategory'])->name('category.update-category');
Route::get('edit-category/{id}', [CategoryController::class, 'editCategory'])->name('category.edit-category');
Route::get('delete-category/{id}', [CategoryController::class, 'deleteCategory'])->name('category.delete-category');

/* 
    *Routes for Product.
*/
Route::get('create-product', [ProductController::class, 'createProduct'])->name('product.create-product');
Route::post('add-product', [ProductController::class, 'addProduct'])->name('product.add-product');
Route::get('show-product', [ProductController::class, 'showProduct'])->name('product.show-product');
Route::get('edit-product/{id}', [ProductController::class, 'editProduct'])->name('product.edit-product');
Route::post('update-product', [ProductController::class, 'updateProduct'])->name('product.update-product');
Route::get('delete-product/{id}', [ProductController::class, 'deleteProduct'])->name('product.delete-product');
