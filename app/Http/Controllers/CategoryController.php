<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * To display the view of dashboard of category.
     *
     * @return \Illuminate\Http\Response
     */
    public function categoryDashboard()
    {
        return view("category.category_dashboard");
    }

    /**
     * To display the view of create of category.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCategory()
    {
        return view("category.create_category");
    }

    /**
     * To display the view of edit of category.
     *
     * @return \Illuminate\Http\Response
     */
    public function editCategory($id)
    {
        $category = Category::find($id);
        return view("category.edit_category", compact('category'));
    }

    /**
     * Delete the category from the id.
     *
     * @param  $id
     * @return return to previous page with message
     */
    public function deleteCategory($id)
    {
        Category::where('id', $id)->delete();
        return response()->json(["status" => '200', "code" => 'Category has been deleted successfully.']);
    }

    
    /**
     * To add new category.
     *
     * @return \Illuminate\Http\Response
     */
    public function addCategory(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'description' => 'required|max:500'
        ]);

        $category = new Category;
        $category->name = $request->name;
        $category->description = $request->description;
        $category->save();
        return redirect('category-dashboard')->with('success_msg', 'Category has been created successfully.');
    }

    /**
     * To update the category deails.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateCategory(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'description' => 'required|max:500'
        ]);

        $category = Category::find($request->id);
        $category->name = $request->name;
        $category->description = $request->description;
        $category->save();
        return redirect('category-dashboard')->with('success_msg', 'Category has been updated successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showCategory()
    {
        $category = Category::all();
        return response()->json($category);
    }
}
