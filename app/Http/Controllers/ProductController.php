<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use DataTables;

class ProductController extends Controller
{
    /**
     * To display the add product screen.
     *
     * @return view
     */
    public function createProduct()
    {
        $categories = Category::all();
        return view("product.create_product", compact('categories'));
    }

    /**
     * To add new product.
     *
     * @return \Illuminate\Http\Response
     */
    public function addProduct(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'category_id' => 'required',
            'price' => 'required',
            'description' => 'required',
            'product_image' => 'required',
        ]);

        $postData = $request->all();
        $defaultDocumentPath = "uploads/";
        if ($request->has('product_image')) {
            $file = $request->file('product_image');
            $fileName = $file->getClientOriginalName();
            $docPath = $defaultDocumentPath . "product_image/" . $fileName;
            Storage::disk('public')->put($docPath, file_get_contents($file));
            $productURL = URL::to(Storage::url($docPath));
        }
        $postData['product_image'] = $productURL;
        Product::create($postData);
        return redirect('dashboard')->with('success_msg', 'Product has been created successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showProduct(Request $request)
    {
        $product = Product::all();
        return datatables()->of($product)
            ->editColumn('category_id', function ($request) {
                return $request->category->name;
            })
            ->toJson();
    }

    /**
     * Edit the category from the product id.
     *
     * @param  $id
     * @return return to previous page with message
     */
    public function editProduct($id)
    {
        $product = Product::find($id);
        $categories = Category::all();
        return view("product.edit_product", compact('product', 'categories'));
    }

    /**
     * To update the product deails of edit of product.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateProduct(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'category_id' => 'required',
            'price' => 'required',
            'description' => 'required'
        ]);

        $postData = $request->all();
        $defaultDocumentPath = "uploads/";
        if ($request->has('product_image')) {
            $file = $request->file('product_image');
            $fileName = $file->getClientOriginalName();
            $docPath = $defaultDocumentPath . "product_image/" . $fileName;
            Storage::disk('public')->put($docPath, file_get_contents($file));
            $productURL = URL::to(Storage::url($docPath));
            $postData['product_image'] = $productURL;
        }

        $product = Product::find($request->id);
        $product->name = $request->name;
        $product->category_id = $request->category_id;
        $product->price = $request->price;
        $product->description = $request->description;
        if ($request->product_image) {
            $product->product_image = $postData['product_image'];
        }
        $product->save();
        return redirect('dashboard')->with('success_msg', 'Product has been updated successfully.');
    }


    /**
     * Delete the category from the product id.
     *
     * @param  $id
     * @return return to previous page with message
     */
    public function deleteProduct($id)
    {
        Product::where('id', $id)->delete();
        return response()->json(["status" => '200', "code" => 'Product has been deleted successfully.']);
    }
}
